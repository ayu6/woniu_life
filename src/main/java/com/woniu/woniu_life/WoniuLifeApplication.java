package com.woniu.woniu_life;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WoniuLifeApplication {

    public static void main(String[] args) {
        SpringApplication.run(WoniuLifeApplication.class, args);
    }

}
